"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddInvoiceInput = exports.ItemInput = exports.UserInput = exports.Item = exports.Invoice = void 0;
const type_graphql_1 = require("type-graphql");
const user_types_1 = require("../user.types");
let Invoice = class Invoice {
};
__decorate([
    type_graphql_1.Field(() => type_graphql_1.ID),
    __metadata("design:type", String)
], Invoice.prototype, "id", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], Invoice.prototype, "name", void 0);
__decorate([
    type_graphql_1.Field(() => user_types_1.User),
    __metadata("design:type", user_types_1.User)
], Invoice.prototype, "customer", void 0);
__decorate([
    type_graphql_1.Field(() => user_types_1.User),
    __metadata("design:type", user_types_1.User)
], Invoice.prototype, "vendor", void 0);
__decorate([
    type_graphql_1.Field(() => Status, { nullable: true }),
    __metadata("design:type", String)
], Invoice.prototype, "status", void 0);
__decorate([
    type_graphql_1.Field(() => [Item]),
    __metadata("design:type", Array)
], Invoice.prototype, "items", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], Invoice.prototype, "currency", void 0);
__decorate([
    type_graphql_1.Field(() => type_graphql_1.Float),
    __metadata("design:type", Number)
], Invoice.prototype, "total", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], Invoice.prototype, "description", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", Date)
], Invoice.prototype, "createdAt", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", Date)
], Invoice.prototype, "updatedAt", void 0);
Invoice = __decorate([
    type_graphql_1.ObjectType()
], Invoice);
exports.Invoice = Invoice;
var Status;
(function (Status) {
    Status["PENDING"] = "pending";
    Status["DELIVERED"] = "delivered";
    Status["SHIPPED"] = "shipped";
})(Status || (Status = {}));
type_graphql_1.registerEnumType(Status, {
    name: 'Status',
    description: 'The basic Status',
});
let Item = class Item {
};
__decorate([
    type_graphql_1.Field(() => type_graphql_1.ID),
    __metadata("design:type", Number)
], Item.prototype, "id", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], Item.prototype, "name", void 0);
__decorate([
    type_graphql_1.Field(() => type_graphql_1.Float),
    __metadata("design:type", Number)
], Item.prototype, "unitPrice", void 0);
__decorate([
    type_graphql_1.Field(() => type_graphql_1.Int),
    __metadata("design:type", Number)
], Item.prototype, "unit", void 0);
Item = __decorate([
    type_graphql_1.ObjectType()
], Item);
exports.Item = Item;
let UserInput = class UserInput {
};
__decorate([
    type_graphql_1.Field(() => type_graphql_1.ID),
    __metadata("design:type", String)
], UserInput.prototype, "id", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], UserInput.prototype, "avatar", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], UserInput.prototype, "name", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], UserInput.prototype, "address", void 0);
UserInput = __decorate([
    type_graphql_1.InputType({ description: 'New User Data' })
], UserInput);
exports.UserInput = UserInput;
let ItemInput = class ItemInput {
};
__decorate([
    type_graphql_1.Field(() => type_graphql_1.ID),
    __metadata("design:type", Number)
], ItemInput.prototype, "id", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], ItemInput.prototype, "name", void 0);
__decorate([
    type_graphql_1.Field(() => type_graphql_1.Float),
    __metadata("design:type", Number)
], ItemInput.prototype, "unitPrice", void 0);
__decorate([
    type_graphql_1.Field(() => type_graphql_1.Int),
    __metadata("design:type", Number)
], ItemInput.prototype, "unit", void 0);
ItemInput = __decorate([
    type_graphql_1.InputType({ description: 'New Item Data' })
], ItemInput);
exports.ItemInput = ItemInput;
let AddInvoiceInput = class AddInvoiceInput {
};
__decorate([
    type_graphql_1.Field(() => type_graphql_1.ID),
    __metadata("design:type", String)
], AddInvoiceInput.prototype, "id", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], AddInvoiceInput.prototype, "name", void 0);
__decorate([
    type_graphql_1.Field(() => UserInput),
    __metadata("design:type", UserInput)
], AddInvoiceInput.prototype, "customer", void 0);
__decorate([
    type_graphql_1.Field(() => UserInput),
    __metadata("design:type", UserInput)
], AddInvoiceInput.prototype, "vendor", void 0);
__decorate([
    type_graphql_1.Field(() => Status, { nullable: true }),
    __metadata("design:type", String)
], AddInvoiceInput.prototype, "status", void 0);
__decorate([
    type_graphql_1.Field(() => [ItemInput]),
    __metadata("design:type", Array)
], AddInvoiceInput.prototype, "items", void 0);
__decorate([
    type_graphql_1.Field(() => type_graphql_1.Float),
    __metadata("design:type", Number)
], AddInvoiceInput.prototype, "total", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", Date)
], AddInvoiceInput.prototype, "createdAt", void 0);
AddInvoiceInput = __decorate([
    type_graphql_1.InputType({ description: 'New recipe data' })
], AddInvoiceInput);
exports.AddInvoiceInput = AddInvoiceInput;
//# sourceMappingURL=invoice.types.js.map