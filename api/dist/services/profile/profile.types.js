"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Comment = exports.Post = exports.Profile = void 0;
const type_graphql_1 = require("type-graphql");
const user_types_1 = require("../user.types");
let Profile = class Profile {
};
__decorate([
    type_graphql_1.Field(() => type_graphql_1.ID),
    __metadata("design:type", String)
], Profile.prototype, "id", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], Profile.prototype, "name", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], Profile.prototype, "bio", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], Profile.prototype, "role", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], Profile.prototype, "avatar", void 0);
__decorate([
    type_graphql_1.Field(() => [Post], { nullable: true }),
    __metadata("design:type", Array)
], Profile.prototype, "posts", void 0);
__decorate([
    type_graphql_1.Field(() => [user_types_1.User], { nullable: true }),
    __metadata("design:type", Array)
], Profile.prototype, "followers", void 0);
__decorate([
    type_graphql_1.Field(() => [user_types_1.User], { nullable: true }),
    __metadata("design:type", Array)
], Profile.prototype, "following", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", Date)
], Profile.prototype, "createdAt", void 0);
Profile = __decorate([
    type_graphql_1.ObjectType()
], Profile);
exports.Profile = Profile;
let Post = class Post {
};
__decorate([
    type_graphql_1.Field(() => type_graphql_1.ID),
    __metadata("design:type", Number)
], Post.prototype, "id", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], Post.prototype, "type", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], Post.prototype, "image", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], Post.prototype, "video", void 0);
__decorate([
    type_graphql_1.Field(() => [String], { nullable: true }),
    __metadata("design:type", Array)
], Post.prototype, "gallery", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], Post.prototype, "numberOflike", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], Post.prototype, "numberOfcomment", void 0);
__decorate([
    type_graphql_1.Field(() => [Comment], { nullable: true }),
    __metadata("design:type", Array)
], Post.prototype, "comments", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", Date)
], Post.prototype, "createdAt", void 0);
Post = __decorate([
    type_graphql_1.ObjectType()
], Post);
exports.Post = Post;
let Comment = class Comment {
};
__decorate([
    type_graphql_1.Field(() => type_graphql_1.ID),
    __metadata("design:type", Number)
], Comment.prototype, "id", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], Comment.prototype, "role", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], Comment.prototype, "avatar", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], Comment.prototype, "username", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], Comment.prototype, "comment", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", Date)
], Comment.prototype, "createdAt", void 0);
Comment = __decorate([
    type_graphql_1.ObjectType()
], Comment);
exports.Comment = Comment;
//# sourceMappingURL=profile.types.js.map