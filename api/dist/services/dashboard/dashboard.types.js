"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Dashboard = exports.CashGraph = exports.ProductsBarGraph = exports.ProductGraph = void 0;
const type_graphql_1 = require("type-graphql");
const apps_types_1 = require("../apps.types");
let ProductGraph = class ProductGraph {
};
__decorate([
    type_graphql_1.Field(() => [String], { nullable: true }),
    __metadata("design:type", Array)
], ProductGraph.prototype, "categories", void 0);
__decorate([
    type_graphql_1.Field(() => [type_graphql_1.Float], { nullable: true }),
    __metadata("design:type", Array)
], ProductGraph.prototype, "products", void 0);
__decorate([
    type_graphql_1.Field(() => [type_graphql_1.Float], { nullable: true }),
    __metadata("design:type", Array)
], ProductGraph.prototype, "views", void 0);
ProductGraph = __decorate([
    type_graphql_1.ObjectType()
], ProductGraph);
exports.ProductGraph = ProductGraph;
let ProductsBarGraph = class ProductsBarGraph {
};
__decorate([
    type_graphql_1.Field(() => [String], { nullable: true }),
    __metadata("design:type", Array)
], ProductsBarGraph.prototype, "labels", void 0);
__decorate([
    type_graphql_1.Field(() => [type_graphql_1.Float], { nullable: true }),
    __metadata("design:type", Array)
], ProductsBarGraph.prototype, "products", void 0);
ProductsBarGraph = __decorate([
    type_graphql_1.ObjectType()
], ProductsBarGraph);
exports.ProductsBarGraph = ProductsBarGraph;
let CashGraph = class CashGraph {
};
__decorate([
    type_graphql_1.Field(() => [String], { nullable: true }),
    __metadata("design:type", Array)
], CashGraph.prototype, "categories", void 0);
__decorate([
    type_graphql_1.Field(() => [type_graphql_1.Float], { nullable: true }),
    __metadata("design:type", Array)
], CashGraph.prototype, "cash", void 0);
CashGraph = __decorate([
    type_graphql_1.ObjectType()
], CashGraph);
exports.CashGraph = CashGraph;
let Dashboard = class Dashboard {
};
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], Dashboard.prototype, "title", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], Dashboard.prototype, "shortDescription", void 0);
__decorate([
    type_graphql_1.Field(() => [apps_types_1.App]),
    __metadata("design:type", Array)
], Dashboard.prototype, "recentApps", void 0);
__decorate([
    type_graphql_1.Field(() => ProductGraph),
    __metadata("design:type", ProductGraph)
], Dashboard.prototype, "productViews", void 0);
__decorate([
    type_graphql_1.Field(() => ProductsBarGraph),
    __metadata("design:type", ProductsBarGraph)
], Dashboard.prototype, "productsBar", void 0);
__decorate([
    type_graphql_1.Field(() => CashGraph),
    __metadata("design:type", CashGraph)
], Dashboard.prototype, "cashFlow", void 0);
Dashboard = __decorate([
    type_graphql_1.ObjectType()
], Dashboard);
exports.Dashboard = Dashboard;
//# sourceMappingURL=dashboard.types.js.map