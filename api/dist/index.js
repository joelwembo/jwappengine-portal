"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const express_1 = __importDefault(require("express"));
const apollo_server_express_1 = require("apollo-server-express");
const type_graphql_1 = require("type-graphql");
const product_resolver_1 = __importDefault(require("./services/product/product.resolver"));
const integration_resolver_1 = __importDefault(require("./services/integration/integration.resolver"));
const profile_resolver_1 = __importDefault(require("./services/profile/profile.resolver"));
const dashboard_resolver_1 = __importDefault(require("./services/dashboard/dashboard.resolver"));
const invoice_resolver_1 = __importDefault(require("./services/invoice/invoice.resolver"));
(() => __awaiter(void 0, void 0, void 0, function* () {
    const app = express_1.default();
    const apolloServer = new apollo_server_express_1.ApolloServer({
        schema: yield type_graphql_1.buildSchema({
            resolvers: [
                product_resolver_1.default,
                integration_resolver_1.default,
                profile_resolver_1.default,
                dashboard_resolver_1.default,
                invoice_resolver_1.default,
            ],
            validate: false,
        }),
        introspection: true,
        playground: true,
    });
    const port = process.env.PORT || 4002;
    const path = '/graphql';
    apolloServer.applyMiddleware({ app, path, cors: true });
    app.listen(port, () => {
        console.log(`server started at http://localhost:${port}/graphql`);
    });
}))();
//# sourceMappingURL=index.js.map